AggroMeter = {}
local Version = "1.2"
local PlayerName = wstring.sub(GameData.Player.name,1,-3)

function AggroMeter.Initialize()
	RegisterEventHandler(TextLogGetUpdateEventId("Chat"), "AggroMeter.OnChatLogUpdated") -- runs the function when Chatlog gets a new text
	AggroMeter.Enabled = true
	CreateWindow("AggroMeter_Button", true)	

	
	
	
	
	AggroMeter.PlayersAggro = {}
	AggroMeter.AggroHolder = {}
	AggroMeter.MobID = {}
	AggroMeter.MobName = {}
	AggroMeter.MobRank	= {}
	AggroMeter.MaxAggro = {}
	AggroMeter.Timers = {}
	AggroMeter.Stacks = {}
	AggroMeter.CombatTime = {}	
	AggroMeter.Fader = {}
	AggroMeter.Listdata = {}
	AggroMeter.SelectedTab = 1
	
	AggroMeter.Color = {DefaultColor.YELLOW,DefaultColor.ORANGE,DefaultColor.RED}
	AggroMeter.Unit = {L"(C)",L"(H)",L"(L)"}
	
	AggroMeter.HideChannel(65)

	if not AggroMeter.Settings then AggroMeter.Settings = {} end
	if not AggroMeter.Settings.Style then AggroMeter.Settings.Style = 1 end
	if not AggroMeter.Settings.ShowRank then AggroMeter.Settings.ShowRank = {false,true,true} end
	if not AggroMeter.Settings.Players then AggroMeter.Settings.Players = 6 end
	if not AggroMeter.Settings.List then AggroMeter.Settings.List = {White={},Black={},Gray={}} end
		
	CreateWindow("AggroMeterGrayWindow", false)
	ButtonSetText("AggroMeterGrayWindowListTab", L"List")
	ButtonSetText("AggroMeterGrayWindowWhiteTab", L"Whitelist")
	ButtonSetText("AggroMeterGrayWindowBlackTab", L"Blacklist")
	ButtonSetPressedFlag( "AggroMeterGrayWindowListTab",true)		
	ButtonSetPressedFlag( "AggroMeterGrayWindowWhiteTab",false)			
	ButtonSetPressedFlag( "AggroMeterGrayWindowBlackTab",false)	
	LabelSetText("AggroMeterGrayWindowLabel",L"AggroMeter Priority List")
	AggroMeter.Update()
end

function AggroMeter.Shutdown()
	UnregisterEventHandler(TextLogGetUpdateEventId("Chat"), "AggroMeter.OnChatLogUpdated")	
end

function AggroMeter.OnChatLogUpdated(updateType, filterType)
	if AggroMeter.Enabled == true then
			if( updateType == SystemData.TextLogUpdate.ADDED ) then 		
				if filterType == SystemData.ChatLogFilters.CHANNEL_9 then	
					local _, filterId, text = TextLogGetEntry( "Chat", TextLogGetNumEntries("Chat") - 1 ) 
					if text:find(L"NPC_AGGRO") then 
						AggroMeter.SplitText(text)
						if AggroMeter.Debug == true then DEBUG(text) end
					end
				end
			end
	end		
end


function AggroMeter.SplitText(text)
	if text ~= nil then
		text = tostring(text)
	end

	xListSplit = StringSplit(text, ";")
	xListSplit[#xListSplit] = nil
	local MobID = tostring(xListSplit[2])
	local MobRank = tostring(xListSplit[3])	
	local MobName = tostring(xListSplit[4])
	
		local IsInLists = ""
		
		for k,v in pairs(AggroMeter.Settings.List) do
			for a,b in pairs(v) do
				if b.Name == towstring(MobName) then
					IsInLists = tostring(k)					
					break
				end		
			end
		end
		
		if IsInLists == "" then		
		if (#AggroMeter.Settings.List.Gray) >= 20 then
			table.remove (AggroMeter.Settings.List.Gray,1)
		end
		table.insert (AggroMeter.Settings.List.Gray,{Name=towstring(MobName),Rank=MobRank,RankN=towstring(AggroMeter.Unit[tonumber(MobRank)])})
		AggroMeter.Update()

		end

	if (AggroMeter.Settings.ShowRank[tonumber(MobRank)] == true or IsInLists == "White") and IsInLists ~= "Black" then
		
		if DoesWindowExist("AggroMeterWindow"..MobID) == false then
		CreateWindowFromTemplate("AggroMeterWindow"..MobID, "AggroMeterWindow", "Root")
		WindowStartAlphaAnimation("AggroMeterWindow"..MobID, Window.AnimationType.SINGLE_NO_RESET, 0, 1, 0.5, false, 0, 0)
			for i=1,6 do	
			
				local LabelW,LabelH = WindowGetDimensions("AggroMeterWindow"..MobID.."_AggroWindow"..i.."TimerBarText")			
				WindowSetDimensions("AggroMeterWindow"..MobID.."_AggroWindow"..i.."TimerBarText",100,LabelH)
				LabelSetText("AggroMeterWindow"..MobID.."_AggroWindow"..i.."Label",L"Aggro"..towstring(i))					
				DynamicImageSetTexture("AggroMeterWindow"..MobID.."_AggroWindow"..i.."Tactic","icon022709",0,0)		
				StatusBarSetMaximumValue("AggroMeterWindow"..MobID.."_AggroWindow"..i.."TimerBar", 100  )
				StatusBarSetForegroundTint( "AggroMeterWindow"..MobID.."_AggroWindow"..i.."TimerBar", DefaultColor.GREEN.r, DefaultColor.GREEN.g, DefaultColor.GREEN.b )
				StatusBarSetBackgroundTint( "AggroMeterWindow"..MobID.."_AggroWindow"..i.."TimerBar", DefaultColor.BLACK.r, DefaultColor.BLACK.g, DefaultColor.BLACK.b )	
				LabelSetText("AggroMeterWindow"..MobID.."_AggroWindow"..i.."TimerBarText",L"")	
			end	
			LabelSetText("AggroMeterWindow"..MobID.."NameLabel",L"MobName "..towstring(MobID))
			AggroMeter.Stacks[MobID] = 1
			AggroMeter.CombatTime[MobID] = 0
		end
		
		AggroMeter.MobID[MobID] = tostring(xListSplit[2])	
		AggroMeter.MobRank[MobID] = tostring(xListSplit[3])	
		AggroMeter.MobName[MobID] = tostring(xListSplit[4])	
		AggroMeter.PlayersAggro[MobID] = (#xListSplit-4)/4
		AggroMeter.AggroHolder[MobID] = {}
		AggroMeter.Fader[MobID] = false
		local Players = 0
		if AggroMeter.PlayersAggro[MobID] < AggroMeter.Settings.Players then Players = AggroMeter.PlayersAggro[MobID] else Players = AggroMeter.Settings.Players end
		
		for i=1,6 do
			LabelSetText("AggroMeterWindow"..MobID.."_AggroWindow"..i.."Label",L"")
			WindowSetShowing("AggroMeterWindow"..MobID.."_AggroWindow"..i,false)	
		end

		AggroMeter.MaxAggro[MobID] = 0

		for i=1,(Players) do
			AggroMeter.AggroHolder[MobID][i] = {}
			AggroMeter.AggroHolder[MobID][i].name = tostring(xListSplit[(1+(i*4))])
			AggroMeter.AggroHolder[MobID][i].aggro = tonumber(xListSplit[(2+(i*4))])
			AggroMeter.AggroHolder[MobID][i].tactic = tonumber(xListSplit[(3+(i*4))])	
			AggroMeter.AggroHolder[MobID][i].career = tonumber(xListSplit[(4+(i*4))])				
			AggroMeter.MaxAggro[MobID] = AggroMeter.AggroHolder[MobID][1].aggro
		end

		for i=1,(Players) do
			LabelSetText("AggroMeterWindow"..MobID.."_AggroWindow"..i.."Label",towstring(AggroMeter.AggroHolder[MobID][i].name))	
			StatusBarSetForegroundTint( "AggroMeterWindow"..MobID.."_AggroWindow"..i.."TimerBar", 255*(((AggroMeter.AggroHolder[MobID][i].aggro/AggroMeter.MaxAggro[MobID])*100)/100), 255*(1-(((AggroMeter.AggroHolder[MobID][i].aggro/AggroMeter.MaxAggro[MobID])*100)/100)), 0)
			StatusBarSetCurrentValue("AggroMeterWindow"..MobID.."_AggroWindow"..i.."TimerBar", (AggroMeter.AggroHolder[MobID][i].aggro/AggroMeter.MaxAggro[MobID])*100 )

				if towstring(AggroMeter.AggroHolder[MobID][i].name) == PlayerName then
						LabelSetTextColor("AggroMeterWindow"..MobID.."_AggroWindow"..i.."Label", 0, 250, 100)
				else
						LabelSetTextColor("AggroMeterWindow"..MobID.."_AggroWindow"..i.."Label", 255, 255, 135)
				end
			
				if AggroMeter.Settings.Style == 1 then
					if	AggroMeter.AggroHolder[MobID][i].aggro > 0 then
						LabelSetText("AggroMeterWindow"..MobID.."_AggroWindow"..i.."TimerBarText",wstring.format(L"%.01f",(AggroMeter.AggroHolder[MobID][i].aggro/AggroMeter.MaxAggro[MobID])*100)..L"%")
					else
						LabelSetText("AggroMeterWindow"..MobID.."_AggroWindow"..i.."TimerBarText",L"0%")
					end
				else
					LabelSetText("AggroMeterWindow"..MobID.."_AggroWindow"..i.."TimerBarText",towstring(AggroMeter.AggroHolder[MobID][i].aggro))
				end
		
			WindowSetShowing("AggroMeterWindow"..MobID.."_AggroWindow"..i,true)	
			WindowSetShowing("AggroMeterWindow"..MobID.."_AggroWindow"..i.."Tactic",AggroMeter.AggroHolder[MobID][i].tactic > 0)	
			WindowSetShowing("AggroMeterWindow"..MobID.."_AggroWindow"..i.."Timer",(LabelGetText("AggroMeterWindow"..MobID.."_AggroWindow"..i.."Label") ~= ""))
			
			local txtr, x, y, disabledTexture = GetIconData (Icons.GetCareerIconIDFromCareerLine(tonumber(AggroMeter.AggroHolder[MobID][i].career)))	
--			CircleImageSetTexture ("AggroMeterWindow"..MobID.."_AggroWindow"..i.."Icon", txtr, 16, 16)
			CircleImageSetTexture("AggroMeterWindow"..MobID.."_AggroWindow"..i.."ButtonIcon",txtr, 16, 16)
			
--[[			
	for row = 1, AggroMeter.PlayersAggro[MobID] do
        local row_mod = math.mod(row, 2)
        color = DataUtils.GetAlternatingRowColor( row_mod )
        
        local targetRowWindow = "AggroMeterWindowRowColor"..row
        WindowSetTintColor("AggroMeterWindowRowColor"..row, color.r, color.g, color.b )
        WindowSetAlpha("AggroMeterWindowRowColor"..row, color.a )
    end
--]]		
						
		end
		
		local Unit = AggroMeter.Unit[tonumber(AggroMeter.MobRank[MobID])]
		LabelSetText("AggroMeterWindow"..MobID.."UnitLabel",Unit)
		LabelSetText("AggroMeterWindow"..MobID.."NameLabel",towstring(AggroMeter.MobName[MobID]))
		local Color = AggroMeter.Color[tonumber(AggroMeter.MobRank[MobID])]
		LabelSetTextColor("AggroMeterWindow"..MobID.."NameLabel",Color.r,Color.g,Color.b)		
		AggroMeter.Timers[MobID] = 3
		WindowSetDimensions("AggroMeterWindow"..MobID,310,45+(30*Players))		
		local StackHeight = 33

		for  k,v in pairs(AggroMeter.Stacks) do
		local width,height = WindowGetDimensions("AggroMeterWindow"..k)
			WindowClearAnchors("AggroMeterWindow"..k)
			WindowAddAnchor("AggroMeterWindow"..k, "topright", "AggroMeter_Button", "topright",0,StackHeight)		
			StackHeight = StackHeight + (height+10)
		end	
	end
end

function AggroMeter.OnMouseOverStart()
	local WinParent = WindowGetParent(SystemData.MouseOverWindow.name)
	local WindowName = towstring(SystemData.MouseOverWindow.name)
	
	if WindowName:match(L"Timer") then
		local MobNumber = 	tostring(WindowName:match(L"AggroMeterWindow([%d.]+)."))
		local TimerNumber = tonumber(WindowName:match(L"_AggroWindow([^%.]+)Timer"))
		local Ttip = L""
			Tooltips.CreateTextOnlyTooltip(SystemData.MouseOverWindow.name,nil)
			Tooltips.SetTooltipText( 1, 1,towstring(AggroMeter.AggroHolder[tostring(MobNumber)][tonumber(TimerNumber)].name))
			Tooltips.SetTooltipColorDef( 1, 1, Tooltips.MAP_DESC_TEXT_COLOR )
			if AggroMeter.AggroHolder[tostring(MobNumber)][tonumber(TimerNumber)].aggro > 0 then
				Ttip =  wstring.format(L"%.01f",(AggroMeter.AggroHolder[tostring(MobNumber)][tonumber(TimerNumber)].aggro/AggroMeter.MaxAggro[tostring(MobNumber)])*100)..L"%"
			else
				Ttip = L"0%"
			end
			Tooltips.SetTooltipText( 1, 3, Ttip)
			Tooltips.SetTooltipText( 2, 1, L"Hatred: "..towstring(AggroMeter.AggroHolder[tostring(MobNumber)][TimerNumber].aggro)..L" / "..towstring(AggroMeter.MaxAggro[tostring(MobNumber)]))		
	elseif WindowName:match(L"Tactic") then
		local MobNumber = 	tostring(WindowName:match(L"AggroMeterWindow([%d.]+)."))
		local TacticNumber = tonumber(WindowName:match(L"_AggroWindow([^%.]+)Tactic"))
		Tooltips.CreateTextOnlyTooltip(SystemData.MouseOverWindow.name,nil)
		Tooltips.SetTooltipText( 1, 1,L"This player is using "..towstring(GetAbilityName(tonumber(AggroMeter.AggroHolder[tostring(MobNumber)][tonumber(TacticNumber)].tactic)))..L" Tactic")
		Tooltips.SetTooltipColorDef( 1, 1, Tooltips.MAP_DESC_TEXT_COLOR )
	elseif WindowName:match(L"AggroMeter_Button") then
			Tooltips.CreateTextOnlyTooltip(SystemData.MouseOverWindow.name,nil)
			Tooltips.SetTooltipText( 1, 1,L"AggroMeter")
			Tooltips.SetTooltipColorDef( 1, 1, Tooltips.MAP_DESC_TEXT_COLOR )
			Tooltips.SetTooltipText( 1, 3, L"Ver: "..towstring(Version))
			Tooltips.SetTooltipText( 2, 1, L"RightClick for options")					
	end
	
	Tooltips.Finalize()    
	Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_TOP )
	
end

function AggroMeter.SelectChar()
	local WinParent = WindowGetParent(SystemData.MouseOverWindow.name)
	local WindowName = towstring(SystemData.MouseOverWindow.name)
	local MobNumber = 	tostring(WindowName:match(L"AggroMeterWindow([%d.]+)."))
	local LabelNumber = tonumber(WindowName:match(L"_AggroWindow([^%.]+)Label"))
		
	WindowSetGameActionData(tostring(WindowName),GameData.PlayerActions.SET_TARGET,0,towstring(AggroMeter.AggroHolder[tostring(MobNumber)][tonumber(LabelNumber)].name))	
end

function AggroMeter.OnTabRBU()

local function MakeCallBack( SelectedOption )
		    return function() AggroMeter.ToggleShow(SelectedOption) end
		end

	EA_Window_ContextMenu.CreateContextMenu( SystemData.MouseOverWindow.name, EA_Window_ContextMenu.CONTEXT_MENU_1,L"Options")
    EA_Window_ContextMenu.AddMenuDivider( EA_Window_ContextMenu.CONTEXT_MENU_1 )	
	if AggroMeter.Enabled == true then  
		EA_Window_ContextMenu.AddMenuItem( L"<icon00057> Enabled" , AggroMeter.ToggeEnable, false, true )
	else
		EA_Window_ContextMenu.AddMenuItem( L"<icon00058> Disabled" , AggroMeter.ToggeEnable, false, true )
	end

	if AggroMeter.Settings.ShowRank[1] == true then
		EA_Window_ContextMenu.AddMenuItem( L"   <icon00057> Champions" , MakeCallBack(1), not AggroMeter.Enabled, true )	
	else
		EA_Window_ContextMenu.AddMenuItem( L"   <icon00058> Champions" , MakeCallBack(1), not AggroMeter.Enabled, true )	
	end
	
	if AggroMeter.Settings.ShowRank[2] == true then
		EA_Window_ContextMenu.AddMenuItem( L"   <icon00057> Heroes" , MakeCallBack(2), not AggroMeter.Enabled, true )	
	else
		EA_Window_ContextMenu.AddMenuItem( L"   <icon00058> Heroes" , MakeCallBack(2), not AggroMeter.Enabled, true )	
	end	

	if AggroMeter.Settings.ShowRank[3] == true then
		EA_Window_ContextMenu.AddMenuItem( L"   <icon00057> Lords" , MakeCallBack(3), not AggroMeter.Enabled, true )	
	else
		EA_Window_ContextMenu.AddMenuItem( L"   <icon00058> Lords" , MakeCallBack(3), not AggroMeter.Enabled, true )	
	end
    EA_Window_ContextMenu.AddMenuDivider( EA_Window_ContextMenu.CONTEXT_MENU_1 )	
	if AggroMeter.Settings.Style == 1 then
		EA_Window_ContextMenu.AddMenuItem( L"Draw Numbers" , AggroMeter.ToggeBar, false, true )	
	else
		EA_Window_ContextMenu.AddMenuItem( L"Draw Percentage" , AggroMeter.ToggeBar, false, true )	
	end
	EA_Window_ContextMenu.AddMenuItem( L"Open Priority List" , AggroMeter.Close, false, true )	
	
	EA_Window_ContextMenu.Finalize()	
end

function AggroMeter.ToggeEnable()
	AggroMeter.Enabled = not AggroMeter.Enabled
	AggroMeter.OnTabRBU()	
end

function AggroMeter.ToggeBar()
	if AggroMeter.Settings.Style == 1 then 
		AggroMeter.Settings.Style = 2 
	else 
		AggroMeter.Settings.Style = 1 
	end
end

function AggroMeter.ToggleShow(SelectedOption)
	AggroMeter.Settings.ShowRank[tonumber(SelectedOption)] = not AggroMeter.Settings.ShowRank[tonumber(SelectedOption)]	
end

function AggroMeter.OnUpdate(timeElapsed)
	for  k,v in pairs(AggroMeter.Timers) do
		LabelSetText("AggroMeterWindow"..k.."CombatLabel",towstring(TimeUtils.FormatClock(AggroMeter.CombatTime[k])))	
			
		AggroMeter.Timers[k] = v - timeElapsed
		AggroMeter.CombatTime[k] = AggroMeter.CombatTime[k] + timeElapsed
		
		if (v <= 0.6) and (AggroMeter.Fader[k] == false) then
			AggroMeter.Fader[k] = true
			WindowStartAlphaAnimation ("AggroMeterWindow"..k, 3, 1.0, 0.0, 0.6, true, 0, 0 )
		end

		if (v <= 0) then
			DestroyWindow( "AggroMeterWindow"..k )
			AggroMeter.Timers[k] = nil
			AggroMeter.Stacks[k] = nil	
			AggroMeter.CombatTime[k] = nil	
			AggroMeter.Fader[k] = nil			
		end
	end
end

function AggroMeter.HideChannel(channelId)
	for _, wndGroup in ipairs(EA_ChatWindowGroups) do 
		if wndGroup.used == true then
			for tabId, tab in ipairs(wndGroup.Tabs) do
				local tabName = EA_ChatTabManager.GetTabName( tab.tabManagerId )		
				if tabName then
					if tab.tabText ~= L"Debug" then
						LogDisplaySetFilterState(tabName.."TextLog", "Chat", channelId, false)
					else
						LogDisplaySetFilterState(tabName.."TextLog", "Chat", channelId, true)
						LogDisplaySetFilterColor(tabName.."TextLog", "Chat", channelId, 168, 187, 160 )
					end
				end
				
			end
			
		end
		
	end	
end

function AggroMeter.Update()
	 AggroMeter.DisplayOrder = {} 
	 if AggroMeter.SelectedTab == 1 then
		AggroMeter.Listdata = AggroMeter.Settings.List.Gray
	 elseif AggroMeter.SelectedTab == 2 then
		AggroMeter.Listdata = AggroMeter.Settings.List.White
	 elseif AggroMeter.SelectedTab == 3 then
		AggroMeter.Listdata = AggroMeter.Settings.List.Black
	 end
	 
	 
	local shtcount = 1
	for k, v in pairs(  AggroMeter.Listdata) do

	table.insert(AggroMeter.DisplayOrder, shtcount)
		shtcount = shtcount+1
	end
	ListBoxSetDisplayOrder("AggroMeterGrayListBox", AggroMeter.DisplayOrder)
	return
end

function AggroMeter.Close()
  WindowSetShowing("AggroMeterGrayWindow",not WindowGetShowing("AggroMeterGrayWindow"))  
end

function AggroMeter.PickedListMenu()
if string.find(SystemData.MouseOverWindow.name,"ListBoxRow") then

local _index = ListBoxGetDataIndex("AggroMeterGrayListBox", WindowGetId(SystemData.MouseOverWindow.name))
local function MakeCallBack( SelectedOption,RowNumber )
		    return function() AggroMeter.AddList(SelectedOption,RowNumber) end
		end
	EA_Window_ContextMenu.CreateContextMenu( SystemData.MouseOverWindow.name, EA_Window_ContextMenu.CONTEXT_MENU_1,L"Options")
    EA_Window_ContextMenu.AddMenuDivider( EA_Window_ContextMenu.CONTEXT_MENU_1 )	
	if AggroMeter.SelectedTab == 1 then
		EA_Window_ContextMenu.AddMenuItem( L"Add to WhiteList" , MakeCallBack(1,_index), false, true )		
		EA_Window_ContextMenu.AddMenuItem( L"Add to BlackList" , MakeCallBack(2,_index), false, true )			
	elseif AggroMeter.SelectedTab == 2 then
		EA_Window_ContextMenu.AddMenuItem( L"Remove from Whitelist" , MakeCallBack(3,_index), false, true )		
		EA_Window_ContextMenu.AddMenuItem( L"Move to BlackList" , MakeCallBack(4,_index), false, true )			
	elseif AggroMeter.SelectedTab == 3 then
		EA_Window_ContextMenu.AddMenuItem( L"Remove from BlackList" , MakeCallBack(5,_index), false, true )		
		EA_Window_ContextMenu.AddMenuItem( L"Move to WhiteList" , MakeCallBack(6,_index), false, true )			
	end
	
	EA_Window_ContextMenu.Finalize()
end	
end

--moving stuff from Priority Lists
function AggroMeter.AddList(SelectedOption,RowNumber)
	if SelectedOption == 1 then
		table.insert (AggroMeter.Settings.List.White,AggroMeter.Settings.List.Gray[tonumber(RowNumber)])
		table.remove (AggroMeter.Settings.List.Gray,RowNumber)	
	elseif SelectedOption == 2 then
		table.insert (AggroMeter.Settings.List.Black,AggroMeter.Settings.List.Gray[tonumber(RowNumber)])
		table.remove (AggroMeter.Settings.List.Gray,RowNumber)	
	elseif SelectedOption == 3 then
		table.remove (AggroMeter.Settings.List.White,RowNumber)
	elseif SelectedOption == 4 then
		table.insert (AggroMeter.Settings.List.Black,AggroMeter.Settings.List.White[tonumber(RowNumber)])
		table.remove (AggroMeter.Settings.List.White,RowNumber)
	elseif SelectedOption == 5 then
		table.remove (AggroMeter.Settings.List.Black,RowNumber)	
	elseif SelectedOption == 6 then
		table.insert (AggroMeter.Settings.List.White,AggroMeter.Settings.List.Black[tonumber(RowNumber)])
		table.remove (AggroMeter.Settings.List.Black,RowNumber)
	end

AggroMeter.Update()
return
end

function AggroMeter.OnTabLBU()
	local tabNumber	= WindowGetId (SystemData.ActiveWindow.name)
	AggroMeter.SelectedTab = tabNumber
	ButtonSetPressedFlag( "AggroMeterGrayWindowListTab",tabNumber==1)		
	ButtonSetPressedFlag( "AggroMeterGrayWindowWhiteTab",tabNumber==2)
	ButtonSetPressedFlag( "AggroMeterGrayWindowBlackTab",tabNumber==3)	
	AggroMeter.Update()
end
